use crate::rule::Rule;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use url::Host;

#[derive(Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct RewriteRules {
    rules: HashMap<String, RewriteRule>,
}

impl RewriteRules {
    pub fn load<'a, R>(reader: &'a mut R) -> Result<RewriteRules, Box<dyn std::error::Error>>
    where
        R: std::io::Read,
    {
        serde_json::from_reader(reader).map_err(Into::into)
    }

    pub fn len(&self) -> usize {
        self.rules.len()
    }

    pub fn into_iter(
        self,
    ) -> impl Iterator<Item = Result<(Host<String>, Rule), Box<dyn std::error::Error>>> {
        self.rules.into_iter().map(|(hoststr, rule)| {
            Host::parse(&hoststr)
                .map_err(Into::into)
                .and_then(|host| rule.try_into().map(|rule| (host, rule)))
        })
    }
}

#[derive(Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct RewriteRule {
    path_rewrites: Vec<String>,
    query_allows: Vec<String>,
}

impl RewriteRule {
    pub fn path_rewrites(&self) -> &[String] {
        &self.path_rewrites
    }

    pub fn query_allows(&self) -> String {
        self.query_allows.join("|")
    }
}
