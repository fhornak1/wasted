use crate::serde_conf;
use crate::serde_conf::RewriteRule;
use regex::Regex;
use std::error::Error;

pub struct Rule {
    path_rewrites: Vec<Regex>,
    params: Regex,
}

impl Rule {
    pub fn from_str<S: AsRef<str>>(
        path_rewrites: &[S],
        param_allow: S,
    ) -> Result<Rule, Box<dyn Error>> {
        path_rewrites
            .iter()
            .map(|rew| Regex::new(rew.as_ref()).map_err(Into::into))
            .collect::<Result<Vec<Regex>, Box<dyn Error>>>()
            .and_then(|rews| {
                Regex::new(param_allow.as_ref())
                    .map_err(Into::into)
                    .map(|params| Rule {
                        path_rewrites: rews,
                        params,
                    })
            })
    }

    pub fn new(path: Vec<Regex>, params: Regex) -> Self {
        Rule {
            path_rewrites: path,
            params,
        }
    }

    pub fn rewrite_path(&self, path: &str) -> String {
        self.path_rewrites.iter().fold(path.to_string(), |pth, re| {
            re.replace_all(&pth, "*").to_string()
        })
    }

    pub fn check_param_allowed(&self, k: &str, v: &str) -> Option<String> {
        let fmt = format!("{}={}", k, v);
        if self.params.is_match(fmt.as_ref()) {
            Some(fmt)
        } else {
            None
        }
    }
}

impl TryFrom<serde_conf::RewriteRule> for Rule {
    type Error = Box<dyn Error>;

    fn try_from(value: RewriteRule) -> Result<Self, Self::Error> {
        Regex::new(value.query_allows().as_ref())
            .map_err(Into::into)
            .and_then(|re| {
                value
                    .path_rewrites()
                    .iter()
                    .map(|res| Regex::new(res.as_str()).map_err(Into::into))
                    .collect::<Result<Vec<Regex>, Box<dyn std::error::Error>>>()
                    .map(|rew_rules| Rule {
                        params: re,
                        path_rewrites: rew_rules,
                    })
            })
    }
}
