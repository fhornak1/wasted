use crate::rule::Rule;
use crate::serde_conf;
use itertools::Itertools;
use std::borrow::Cow;
use std::collections::HashMap;
use std::error::Error;
use std::ops::Deref;
use trie_rs::{Trie, TrieBuilder};
use url::{Host, Url};
use urlencoding;

pub struct Dewastator {
    search: Trie<String>,
    rules: HashMap<Vec<String>, Rule>,
}

impl Dewastator {
    pub fn dewaste(&self, url: &str) -> Result<String, Box<dyn Error>> {
        let decoded: Result<Cow<'_, str>, Box<dyn Error>> =
            urlencoding::decode(url.into()).map_err(Into::into);
        decoded
            .and_then(|urlstr| Url::parse(urlstr.deref()).map_err(Into::into))
            .and_then(|url| {
                self.find_by_host(&url)
                    .map_or(Ok(url.clone()), |rule| dewaste(&url, rule))
            })
            .map(|u| urlencoding::encode(u.as_str()).to_string())
    }

    fn find_by_host(&self, url: &Url) -> Option<&Rule> {
        url.host()
            .map(host_to_fqdn)
            .and_then(|fqdn| self.longest_prefix(fqdn))
    }

    fn longest_prefix(&self, fqdn: Vec<String>) -> Option<&Rule> {
        self.search
            .common_prefix_search(fqdn)
            .iter()
            .max_by_key(|b| b.len())
            .and_then(|key| self.rules.get(key))
    }
}

pub fn dewaste(url: &Url, rule: &Rule) -> Result<Url, Box<dyn Error>> {
    let path: String = rule.rewrite_path(url.path());
    let mut new_u = url.clone();
    new_u.set_path(path.as_ref());
    new_u.set_fragment(None);

    if path == url.path() {
        let query: String = url
            .query_pairs()
            .map(|(k, v)| rule.check_param_allowed(k.as_ref(), v.as_ref()))
            .fold("".to_string(), |query, param| match param {
                None => query,
                Some(param) if query.is_empty() => query + param.as_ref(),
                Some(param) => query + "&" + param.as_ref(),
            });
        new_u.set_query(Some(query.as_ref()))
    } else {
        new_u.set_query(None)
    }

    if let Some(Host::Domain(domain)) = url.host() {
        new_u.set_host(Some(reduce_hostname_to_domain(domain).as_ref()))?
    }

    Ok(new_u)
}

fn host_to_fqdn<S: AsRef<str>>(host: Host<S>) -> Vec<String> {
    match host {
        Host::Domain(domain) => domain_to_fqdn(domain.as_ref()),
        Host::Ipv4(v4) => vec![v4.to_string()],
        Host::Ipv6(v6) => vec![v6.to_string()],
    }
}

fn domain_to_fqdn(host: &str) -> Vec<String> {
    host.trim_start_matches(".")
        .trim_end_matches(".")
        .split(".")
        .collect::<Vec<&str>>()
        .iter()
        .rev()
        .map(|s| s.to_string())
        .collect::<Vec<String>>()
}

fn reduce_hostname_to_domain(host: &str) -> String {
    host.trim_end_matches(".")
        .rsplitn(3, ".")
        .collect::<Vec<&str>>()
        .iter()
        .take(2)
        .rfold(String::new(), |acc, d| {
            if acc.is_empty() {
                acc + d
            } else {
                acc + "." + d
            }
        })
}

pub struct Builder {
    trie_builder: TrieBuilder<String>,
    rules_set: HashMap<Vec<String>, Rule>,
}

impl Builder {
    pub fn new() -> Builder {
        Builder {
            trie_builder: TrieBuilder::new(),
            rules_set: HashMap::new(),
        }
    }

    pub fn with_capacity(capacity: usize) -> Builder {
        Builder {
            trie_builder: TrieBuilder::new(),
            rules_set: HashMap::with_capacity(capacity),
        }
    }

    fn insert_raw(mut self, fqdn: Vec<String>, rule: Rule) -> Self {
        self.trie_builder.push(fqdn.clone());
        self.rules_set.insert(fqdn, rule);
        self
    }

    pub fn insert(self, host: Host<String>, rule: Rule) -> Self {
        self.insert_raw(host_to_fqdn(host), rule)
    }

    pub fn from_reader<R>(reader: &mut R) -> Result<Builder, Box<dyn Error>>
    where
        R: std::io::Read,
    {
        let rules = serde_conf::RewriteRules::load(reader)?;
        let cap = rules.len();
        rules
            .into_iter()
            .fold_ok(Builder::with_capacity(cap), |builder, (host, rule)| {
                builder.insert(host, rule)
            })
    }

    pub fn build(self) -> Dewastator {
        Dewastator {
            search: self.trie_builder.build(),
            rules: self.rules_set,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use regex::Regex;
    use std::net::Ipv4Addr;
    use std::str::FromStr;

    #[test]
    fn test_reduce_hostname_to_domain() {
        assert_eq!(
            reduce_hostname_to_domain("novinky.seznam.cz"),
            String::from("seznam.cz")
        )
    }

    #[test]
    fn test_reduce_hostname_to_domain_only_domain() {
        assert_eq!(
            reduce_hostname_to_domain("seznam.cz"),
            String::from("seznam.cz")
        )
    }

    #[test]
    fn test_reduce_hostname_to_domain_tld() {
        assert_eq!(reduce_hostname_to_domain("cz"), String::from("cz"))
    }

    #[test]
    fn test_reduce_very_long_hostname() {
        assert_eq!(
            reduce_hostname_to_domain("very.long.hostname.for.domain.com"),
            String::from("domain.com")
        )
    }

    #[test]
    fn test_with_dots() {
        assert_eq!(
            reduce_hostname_to_domain(".seznam.cz"),
            String::from("seznam.cz")
        );
        assert_eq!(
            reduce_hostname_to_domain(".seznam.cz."),
            String::from("seznam.cz")
        );
        assert_eq!(reduce_hostname_to_domain("..cz"), String::from("cz"))
    }

    #[test]
    fn test_host_to_fqdn() {
        assert_eq!(
            host_to_fqdn(Host::Domain("novinky.seznam.cz")),
            vec!["cz".to_owned(), "seznam".to_owned(), "novinky".to_owned()]
        );
        assert_eq!(host_to_fqdn(Host::Domain("cz")), vec!["cz".to_owned()])
    }

    #[test]
    fn test_host_to_fqdn_malformed() {
        assert_eq!(
            host_to_fqdn(Host::Domain(".novinky.seznam.cz")),
            vec!["cz".to_owned(), "seznam".to_owned(), "novinky".to_owned()]
        );
        assert_eq!(host_to_fqdn(Host::Domain("cz.")), vec!["cz".to_owned()])
    }

    #[test]
    fn test_host_to_fqdn_ip() {
        assert_eq!(
            host_to_fqdn(Host::<String>::Ipv4(
                Ipv4Addr::from_str("127.0.0.1").unwrap()
            )),
            vec!["127.0.0.1"]
        )
    }

    #[test]
    fn test_dewaste_path_rewrite() {
        let rule = Rule::new(
            vec![Regex::new(r"user/\d+").unwrap()],
            Regex::new(r"tst=\d+|service=\w+").unwrap(),
        );

        assert_eq!(
            dewaste(
                &Url::parse("https://seznam.cz/account/user/1234?tst=1234&service=hp&uid=1234")
                    .unwrap(),
                &rule
            )
            .unwrap()
            .to_string(),
            "https://seznam.cz/account/*".to_string()
        )
    }

    #[test]
    fn test_dewaste_param_allow() {
        let rule = Rule::new(
            vec![Regex::new(r"user/\d+").unwrap()],
            Regex::new(r"tst=\d+|service=\w+").unwrap(),
        );

        assert_eq!(
            dewaste(
                &Url::parse("https://seznam.cz/hp?tst=1234&service=hp&uid=1234").unwrap(),
                &rule
            )
            .unwrap()
            .to_string(),
            "https://seznam.cz/hp?tst=1234&service=hp".to_string()
        )
    }

    #[test]
    fn test_dewaste_strip_subdomain_and_fragment() {
        let rule = Rule::new(
            vec![Regex::new(r"user/\d+").unwrap()],
            Regex::new(r"tst=\d+|service=\w+|uid=\d+").unwrap(),
        );

        assert_eq!(
            dewaste(
                &Url::parse("https://novinky.seznam.cz/nova-novinka-1234?uid=1234#scrol-on-title")
                    .unwrap(),
                &rule
            )
            .unwrap()
            .to_string(),
            "https://seznam.cz/nova-novinka-1234?uid=1234".to_string()
        )
    }
}
