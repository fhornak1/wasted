# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Created dewaste function, which normalizes url using set of regex rules.
- Create Dewastator that stores rules for urls in prefix tree.
- Read configuration from json file, or stream, or anything that could be read.