# wasted
**🚀 Blazing fast 🦀 rust library for normalizing urls**

Normalizes url address with predefined set of rules.

This library toy project created in attempt to mock colleagues, how simply you can write
such library in 🦀 rust.

## Specification
`dewaste`
1. Remove all subdomains from url
2. Remove fragment
3. Filter query parameters using regex allow-list.
4. Substitute path segments (substrings), that matches at least one rewrite rule regex with `*`.
5. If path is modified, then drop all query parameters.

`Dewastator`
1. Searches for rules of this url, using domain prefix tree.
2. apply dewaste on provided url address with best set of rules (i.e. longest common parent).

## Usage
```rust
use wasted::dewaste::Builder;
use wasted::rule::Rule;

fn main() -> () {
    let dewastator = Builder::new()
        .insert(
            Host::Domain("https://subdomain.example.com"),
            Rule::from_str(vec![r"user/\d+", r"account/\d+"], r"tst=\d+").unwrap())
        .insert(
            Host::Domain("https://example.com"),
            Rule::from_str(vec![], r"tst=\d+"))
        .build();
    
    assert_eq!(
        dewastator.dewaste(
            Url::from_str("https://my.subdomain.example.com/user/1234/account/12345?tst=1234567&theme=dark#some-title").unwrap()).unwrap().as_str(),
        "https://example.com/*/*?tst=123456"
    )
}
```
The rules can be loaded from json file, with following scheme:
```json
{
  "https://subdomain.example.com": {
    "path_rewrites": ["user/\\d+", "account/\\d+"],
    "query_allows": ["tst=\\d+"]
  },
  "https://example.com": {
    "path_rewrites": [],
    "query_allows": ["tst=\\d+"]
  }
}
```
